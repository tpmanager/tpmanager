import { validateCredentials, getAuthorizationHeader } from '../api/sessionApi';
import * as types from './actionTypes';
import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions';
import * as teamActions from './teamActions';

export function requestLogin(credential) {
  return {
    type: types.LOGIN_REQUEST,
    isFetching: true,
    isAuthenticated: false,
    credential
  };
}

export function receiveLogin(user) {
  return {
    type: types.LOGIN_SUCCESS,
    isFetching: false,
    isAuthenticated: true,
    id_token: user.Acid,
    user
  };
}

export function loginError(response) {
  return {
    type: types.LOGIN_FAILURE,
    isFetching: false,
    isAuthenticated: false,
    message: response.status + ' : ' + response.statusText
  };
}

export function loginUser(credential) {

  return dispatch => {
    dispatch(beginAjaxCall());
    dispatch(requestLogin(credential));


    return validateCredentials(credential)
      .then(response => {
        if (!response.ok) {
          dispatch(loginError(response));
          return Promise.reject(response);
        }
        response.json().then(user => ({ user, response }))
          .then(({ user, response }) => {
            if (!response.ok) {
              dispatch(loginError(user.message));
              return Promise.reject(user);
            } else {
              localStorage.setItem('Acid', user.Acid);
              localStorage.setItem('LoggedUser', JSON.stringify(user.LoggedUser));
              localStorage.setItem('Credential', getAuthorizationHeader(credential.username, credential.password));

              dispatch(teamActions.loadTeams());
              dispatch(receiveLogin(user));
            }
          });
      }
      ).catch(error => {
        dispatch(ajaxCallError(error));
        //throw (error);
      });
  };
}

export function requestLogout() {
  return {
    type: types.LOGOUT_REQUEST,
    isFetching: true,
    isAuthenticated: true
  };
}

export function receiveLogout() {
  return {
    type: types.LOGOUT_SUCCESS,
    isFetching: false,
    isAuthenticated: false
  };
}

export function logoutUser() {
  return dispatch => {
    dispatch(beginAjaxCall());
    dispatch(requestLogout());

    localStorage.removeItem('Acid');
    localStorage.removeItem('LoggedUser');
    localStorage.removeItem('Credential');

    dispatch(receiveLogout());
  };
}
