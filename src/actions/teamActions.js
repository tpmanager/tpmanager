import { getTeams } from '../api/iterationApi';
import * as types from './actionTypes';
import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions';

export function loadTeamsSuccess(teams) {
  return { type: types.LOAD_TEAMS_SUCCESS, teams };
}

export function loadTeams() {
  return dispatch => {

    dispatch(beginAjaxCall()); // show loading

    return getTeams()
      .then(response => {
        if (!response.ok) {
          //dispatch(loginError(response));
          return Promise.reject(response);
        }
        response.json().then(json => ({ json, response }))
          .then(({ json, response }) => {
            if (!response.ok) {
              //dispatch(loginError(user.message));
              return Promise.reject(json);
            } else {
              dispatch(loadTeamsSuccess(json.Items));
            }
          });
      }
      ).catch(error => {
        dispatch(ajaxCallError(error));
        //throw (error);
      });

  };
}
