import { getIterations, getTeamIterations } from '../api/iterationApi';
import * as types from './actionTypes';
import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions';

export function loadIterationsSuccess(items) {
  return { type: types.LOAD_ITERATION_SUCCESS, items };
}

export function resetIterations() {
  return { type: types.RESET_ITERATION };
}

export function setSelection(selection) {
  return { type: types.SET_SELECTION, selection };
}

export function loadIterations(iterationName, teamName) {
  return dispatch => {
    dispatch(beginAjaxCall()); // show loading

    return getIterations('US', iterationName, teamName)
      .then(response => response.json())
      .then(json => {
        let items = json.Items;

        return getIterations('BUG', iterationName, teamName)
          .then(response => response.json())
          .then(json => {
            let result = json.Items.concat(items);
            result.sort(function (a, b) { return a.NumericPriority - b.NumericPriority; });

            dispatch(loadIterationsSuccess(result));
          })
          .catch(error => {
            dispatch(ajaxCallError(error));
            throw (error);
          });
      })
      .catch(error => {
        dispatch(ajaxCallError(error));
        throw (error);
      });
  };
}

export function loadTeamiterationsSucess(teamIterations) {
  return { type: types.LOAD_TEAMITERATIONS_SUCCESS, teamIterations };
}

export function resetTeamiterations() {
  return { type: types.RESET_TEAMITERATIONS };
}

export function loadTeamiterations(teamId) {
  return dispatch => {
    dispatch(beginAjaxCall()); // show loading

    return getTeamIterations(teamId)
      .then(response => response.json())
      .then(json => {
        dispatch(loadTeamiterationsSucess(json.Items));
      })
      .catch(error => {
        dispatch(ajaxCallError(error));
        throw (error);
      });
  };
}
