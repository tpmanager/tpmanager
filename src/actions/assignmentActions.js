import { getAssignments } from '../api/iterationApi';
import * as types from './actionTypes';
import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions';

export function loadAssignmentsSuccess() {
  return { type: types.LOAD_ASSIGNMENTS_SUCCESS };
}

export function loadAssignments(assignableId) {
  return dispatch => {

    return getAssignments(assignableId);
  };
}
