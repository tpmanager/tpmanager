import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/App';
import HomePage from './components/home/HomePage';
import AboutPage from './components/about/AboutPage';
import IterationPage from './components/iteration/IterationPage';
import LoginPage from './components/login/LoginPage';
import { isLoggedIn } from './api/auth';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={IterationPage} onEnter={requireAuth} />
    <Route path="iteration" component={IterationPage} onEnter={requireAuth} />
    <Route path="home" component={HomePage} />
    <Route path="about" component={AboutPage} />
    <Route path="login" component={LoginPage} />
  </Route>
);

function requireAuth(nextState, replace) {
  if (!isLoggedIn()) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }
    });
  }
}
