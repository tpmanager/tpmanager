export function isLoggedIn() {
  return !!localStorage.getItem('Acid');
}

export function getAcid() {
  return localStorage.getItem('Acid') || '';
}

export function getLoggedUser() {
  return JSON.parse(localStorage.getItem('LoggedUser') || '{}');
}

export function getCredential() {
  return localStorage.getItem('Credential') || '';
}

export function getResourceUrl(resourceType, id) {
  return "http://tp.intelex.com/RestUI/Board.aspx?acid=" + getAcid() + "#page=" + resourceType + "/" + id;
}