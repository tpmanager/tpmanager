import * as api from "./configApi";
import * as auth from "./auth";
import "isomorphic-fetch";

function getUrl(entityName) {
  return api.BASEURL + entityName + "/?format=json";
}

function getRequest(url, param) {

  return fetch(
    `${url}${param}`,
    {
      method: 'GET',
      headers: {
        Authorization: auth.getCredential(),
        'Content-Type': 'application/json'
      }
    }
  );
}

export function getIterations(type, iterationName, teamName) {
  let _param = "";
  let baseUrl = type === "US" ? getUrl(api.USERSTORIES) : getUrl(api.BUGS);
  baseUrl += "&include=[Id,Name,Effort,NumericPriority,EntityState,CustomFields]";

  if (iterationName !== undefined) {
    if (_param === "") _param = `&where=%28TeamIteration.Id eq '${iterationName}'%29`;
  }
  if (teamName !== undefined) {
    if (_param === "") _param = `&where=%28Team.Id eq '${teamName}'%29`;
    else _param += ` and %28Team.Id eq '${teamName}'%29`;
  }

  return getRequest(baseUrl, _param);
}

export function getTeams() {

  let baseUrl = getUrl(api.TEAMS) + "&include=[Id,Name,NumericPriority]";

  return getRequest(baseUrl, "");
}

export function getTeamIterations(teamId) {
  let _param = "";
  let baseUrl = getUrl(api.TEAMITERATIONS) + "&include=[Id,Name,NumericPriority]";

  if (teamId !== undefined) {
    _param = `&where=%28Team.Id eq ${teamId}%29`;
  }

  return getRequest(baseUrl, _param);
}

export function getAssignments(assignableId) {

  let _param = "";
  let baseUrl = getUrl(api.ASSIGNMENTS);

  if (assignableId !== undefined) {
    _param = `&where=%28Assignable.Id eq ${assignableId}%29`;
  }

  return getRequest(baseUrl, _param);
}