export const BASEURL = "http://tp.intelex.com/api/v1/";

export const USERSTORIES = "Userstories";
export const BUGS = "Bugs";
export const TEAMS = "Teams";
export const TEAMITERATIONS = "TeamIterations";
export const ASSIGNMENTS = "Assignments";
export const CONTEXT = "Context";