import * as api from "./configApi";
import "isomorphic-fetch";
import base64 from 'base-64';

export function getAuthorizationHeader(username, password) {
  let encodedCred = base64.encode(`${username}:${password}`);
  return `Basic ${encodedCred}`;
}

function getUrl(entityName) {
  return api.BASEURL + entityName + "/?format=json";
}

function getRequest(url, credential) {

  return fetch(
    `${url}`,
    {
      method: 'GET',
      headers: {
        Authorization: getAuthorizationHeader(credential.username, credential.password),
        'Content-Type': 'application/json'
      }
    }
  );
}

export function validateCredentials(credential) {

  let baseUrl = getUrl(api.CONTEXT);
  return getRequest(baseUrl, credential);
}