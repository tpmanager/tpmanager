import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';

class AboutPage extends Component {
  render() {
    return (
      <DocumentTitle title={`About`}>
      <div className="container">
        <h3>About</h3>
        <p>This application uses React, Redux, React Router and variety of othere helpfull libraries to show a scrum board using TP webAPIs</p>
      </div>
      </DocumentTitle>
    );
  }
}

export default AboutPage;
