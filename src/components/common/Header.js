import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';
import Loading from './Loading';
import { Nav, Navbar, NavItem } from 'react-bootstrap';
import { IndexLinkContainer, LinkContainer } from 'react-router-bootstrap';
import { logoutUser } from '../../actions/sessionActions';

class Header extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.logOut = this.logOut.bind(this);
  }

  logOut(event) {
    event.preventDefault();
    this.props.dispatch(logoutUser());
  }

  render() {
    const { loading, isAuthenticated, LoggedUser } = this.props;
    let topHoverBar = {
      menuStyle: {
        backgroundColor: 'WhiteSmoke',
        height: 6,
        cursor: 'pointer'
      }
    };
    return (
      <div id="top-menu-bar" style={topHoverBar.menuStyle} >
        <Navbar fixedTop hidden>
          <Navbar.Header>
            <Navbar.Toggle />
            <Navbar.Brand>
              <a href="#">Web TP Manager</a>
            </Navbar.Brand>
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              <LinkContainer to="/home">
                <NavItem>Home</NavItem>
              </LinkContainer>
              {isAuthenticated &&
                <IndexLinkContainer to="/iteration">
                  <NavItem>Scrum Board</NavItem>
                </IndexLinkContainer>
              }
              {!isAuthenticated &&
                <LinkContainer to="/login" >
                  <NavItem>Login</NavItem>
                </LinkContainer>
              }
              <LinkContainer to="/about" >
                <NavItem>About</NavItem>
              </LinkContainer>
            </Nav>
            <Nav pullRight onSelect={this.onSelect}>
              {isAuthenticated &&
                <Navbar.Text>
                  User : <b>{LoggedUser.FirstName} {LoggedUser.LastName}</b>
                </Navbar.Text>
              }
              {isAuthenticated &&
                <NavItem onClick={this.logOut}>Logout</NavItem>
              }

            </Nav>

            <br />
            {loading && <Loading interval={100} dots={20} />}
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}

Header.propTypes = {
  dispatch: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  LoggedUser: PropTypes.object.isRequired
};

export default Header;
