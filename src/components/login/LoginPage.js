import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as sessionActions from '../../actions/sessionActions';
import { Form, Button, FormControl, Label, FormGroup, ControlLabel } from 'react-bootstrap';
import DocumentTitle from 'react-document-title';

class Login extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: ''
    };

    this.handleClick = this.handleClick.bind(this);
    this.onUsernameChange = this.onUsernameChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
  }

  onUsernameChange(e) {
    this.setState({ username: e.target.value });
  }

  onPasswordChange(e) {
    this.setState({ password: e.target.value });
  }

  handleClick(event) {
    let username = this.state.username.trim();
    let password = this.state.password.trim();
    const credential = { username: username, password: password };
    this.props.actions.loginUser(credential);
  }

  render() {
    const { errorMessage } = this.props;

    return (
      <DocumentTitle title={`Login`}>
        <div className="container">
          <h3>Login</h3>
          <form autoComplete="off">
            <FormGroup>
              <ControlLabel>User Name</ControlLabel>
              <FormControl type="text" placeholder="User Name" onChange={this.onUsernameChange} />
            </FormGroup>
            <FormGroup>
              <ControlLabel>Password</ControlLabel>
              <FormControl type="password" placeholder="Password" onChange={this.onPasswordChange} />
            </FormGroup>
            <Button onClick={this.handleClick} className="btn btn-primary">Login</Button>
            <FormGroup>
              {errorMessage &&
                <FormGroup controlId="formValidationError2" validationState="error">
                  <ControlLabel>{errorMessage}</ControlLabel>
                </FormGroup>
              }
            </FormGroup>
          </form>
        </div>
      </DocumentTitle>
    );
  }
}

Login.propTypes = {
  actions: PropTypes.object.isRequired,
  errorMessage: PropTypes.string,
  loading: PropTypes.bool.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    errorMessage: state.session.errorMessage,
    loading: state.ajaxCallsInProgress > 0
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(sessionActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);