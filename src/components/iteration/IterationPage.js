import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as iterationActions from '../../actions/iterationActions';
import IterationList from './IterationList';
import * as util from './util';
import Notes from './Notes';
import SelectInput from '../common/SelectInput';
import initialState from '../../reducers/initialState';
import DocumentTitle from 'react-document-title';
import _ from 'underscore';
import { Grid, Row, Col, Panel, Checkbox, FormGroup, ControlLabel, FormControl, Button, Badge } from 'react-bootstrap';

class IterationPage extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.openIcon = "▼";
        this.closeIcon = "►";

        this.state = {
            selection: Object.assign({}, this.props.selection),
            refresh: this.props.selection.teamId !== "" && this.props.selection.teamIterationId !== "",
            refreshInterval: 10,
            open: true,
            collapseIcon: this.openIcon
        };

        this.onClickRefresh = this.onClickRefresh.bind(this);
        this.onClickTeam = this.onClickTeam.bind(this);
        this.toggleAutoRefresh = this.toggleAutoRefresh.bind(this);
        this.onChangeAutoRefreshInterval = this.onChangeAutoRefreshInterval.bind(this);

        this.onPanelHeaderClick = this.onPanelHeaderClick.bind(this);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    setRefreshInterval(interval) {
        this.interval = setInterval(() => {
            if (this.state.refresh) {
                this.props.actions.loadIterations(this.state.selection.teamIterationId, this.state.selection.teamId);
            }
        }, (interval * 1000));
    }

    onPanelHeaderClick() {
        this.setState({
            open: !this.state.open,
            collapseIcon: (this.state.collapseIcon == this.closeIcon) ? this.openIcon : this.closeIcon
        });
    }

    toggleAutoRefresh(event) {
        if (this.state.checkboxState) {
            clearInterval(this.interval);
        } else {
            this.setRefreshInterval(this.state.refreshInterval);
        }
        this.setState({
            checkboxState: !this.state.checkboxState
        });
    }

    onChangeAutoRefreshInterval(event) {
        const interval = event.target.value !== "" ? event.target.value : 10;
        this.setState({ refreshTime: interval });
        clearInterval(this.interval);
        this.setRefreshInterval(interval);
    }

    onClickRefresh() {
        this.props.actions.loadIterations(this.state.selection.teamIterationId, this.state.selection.teamId);
    }

    onClickTeam(event) {

        const field = event.target.name;
        const value = event.target.value;
        let selection = this.state.selection;
        selection[field] = value;
        if (field === "teamId") {
            selection.teamIterationId = "";
        }

        let newSelection = Object.assign({}, this.state.selection, { [field]: value });
        this.props.actions.setSelection(newSelection);
        this.setState({ refresh: false });

        if (value !== "") {
            if (field === "teamId") {
                this.props.actions.resetIterations();
                this.props.actions.loadTeamiterations(selection.teamId);
            } else if (field === "teamIterationId") {
                this.setState({ refresh: true });
                this.props.actions.loadIterations(selection.teamIterationId, selection.teamId);
            }
        } else {
            if (field === "teamId") {
                this.props.actions.resetTeamiterations();
            }
            this.props.actions.resetIterations();
        }
    }

    render() {
        const { items } = this.props;
        const { teams } = this.props;
        const { teamIterations } = this.props;

        let commitedEfforts = 0;
        items.map(item =>
            commitedEfforts += (item.Effort * util.effortPercentage(item))
        );

        let todoEfforts = 0;
        _.filter(items, util.isTodoItem).map(item => todoEfforts += item.Effort);

        let inProgressEfforts = 0;
        _.filter(items, util.isInprogressItem).map(item => inProgressEfforts += item.Effort);

        let completedEfforts = 0;
        _.filter(items, util.isCompletedItem).map(item => completedEfforts += item.Effort);

        return (
            <DocumentTitle title={`Scrum Board`}>
                <div >
                    <Grid style={{ width: '98%' }}>
                        <Row>
                            <Col sm={4} >
                                <SelectInput
                                    name="teamId"
                                    label="Team"
                                    defaultOption="Select Team"
                                    options={teams}
                                    value={this.state.selection.teamId}
                                    onChange={this.onClickTeam}
                                />
                            </Col>
                            <Col sm={4}>
                                <SelectInput
                                    name="teamIterationId"
                                    label="Iteration"
                                    defaultOption="Select Iteration"
                                    options={teamIterations}
                                    value={this.state.selection.teamIterationId}
                                    onChange={this.onClickTeam} />
                            </Col>
                            <Col sm={4}>
                                <Row>
                                    <Col sm={3}>
                                        <div className="form-group">
                                            <div className="field">
                                                <Button disabled={!this.state.refresh || this.props.loading} onClick={this.onClickRefresh}>Refresh</Button>
                                            </div>
                                        </div>
                                    </Col>
                                    {this.state.refresh &&
                                        <Col sm={5}>
                                            <div className="form-group">
                                                <div className="field">

                                                    <Checkbox disabled={!this.state.refresh || this.props.loading} onClick={this.toggleAutoRefresh}>Auto Refresh</Checkbox>
                                                </div>
                                            </div>
                                        </Col>
                                    }
                                    {this.state.checkboxState &&
                                        <Col sm={4}>
                                            <FormGroup controlId="autoRefreshInterval">
                                                <FormControl type="number" min="10" placeholder="Interval" defaultValue="10" onChange={this.onChangeAutoRefreshInterval} />
                                            </FormGroup>
                                        </Col>
                                    }
                                </Row>
                            </Col>
                        </Row>
                        {items.length > 0 &&
                            <Row>
                                <Col sm={4}>
                                    <Panel header={<strong>To Do <Badge pullRight> {todoEfforts} </Badge></strong>} style={{ textAlign: 'center', opacity: 0.9 }} bsStyle="danger">
                                        <IterationList status="TODO" items={items} />
                                    </Panel>
                                </Col>
                                <Col sm={4}>
                                    <Panel header={<strong>In Progress <Badge pullRight> {inProgressEfforts} </Badge></strong>} style={{ textAlign: 'center', opacity: 0.9 }} bsStyle="warning">
                                        <IterationList status="INPROGRESS" items={items} />
                                    </Panel>
                                </Col>
                                <Col sm={4}>
                                    <Panel header={<strong>Completed <Badge pullRight> {completedEfforts} </Badge></strong>} style={{ textAlign: 'center', opacity: 0.9 }} bsStyle="success">
                                        <Panel collapsible
                                            expanded={this.state.open}
                                            header={<div style={{ height: '10pt' }}>
                                                <div style={{ float: 'left', cursor: 'pointer', width: '15pt' }} onClick={this.onPanelHeaderClick}>
                                                    {this.state.collapseIcon}
                                                </div>
                                                <div style={{ float: 'left' }}><strong>Notes</strong></div>
                                                <Badge pullRight style={{ opacity: '0.5' }}> {commitedEfforts} </Badge>
                                                <div style={{ opacity: '0.7', float: 'right' }}>
                                                    <strong>Committed &nbsp;&nbsp;</strong>
                                                </div>
                                            </div>}
                                            style={{ textAlign: 'left' }}
                                            bsStyle="success">
                                            <Notes teamId={this.state.selection.teamId} teamIterationId={this.state.selection.teamIterationId} />
                                        </Panel>
                                        <IterationList status="COMPLETED" items={items} />
                                    </Panel>
                                </Col>
                            </Row>
                        }
                    </Grid>
                </div>
            </DocumentTitle>
        );
    }
}

IterationPage.propTypes = {
    items: PropTypes.array.isRequired,
    teams: PropTypes.array.isRequired,
    teamIterations: PropTypes.array.isRequired,
    selection: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {

    const teamsDropdown = state.teams.map(team => {
        return {
            value: team.Id,
            text: team.Name
        };
    });

    const teamIterationsDropsown = state.teamIterations.map(team => {
        return {
            value: team.Id,
            text: team.Name
        };
    });

    return {
        items: state.items,
        teams: teamsDropdown,
        teamIterations: teamIterationsDropsown,
        selection: state.selection,
        loading: state.ajaxCallsInProgress > 0
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(iterationActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(IterationPage);
