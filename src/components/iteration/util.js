
export function isTodoItem(x) {
  switch (x.EntityState.Name) {
    case "New":
    case "Ready For Testing":
    case "Confirmed on Feature":
    case "More Info Required":
      return true;

    default:
      return false;
  }
}

export function isInprogressItem(x) {
  switch (x.EntityState.Name) {
    case "In Testing":
    case "In Development":
    case "Under Review":
      return true;

    default:
      return false;
  }
}

export function isCompletedItem(x) {
  switch (x.EntityState.Name) {
    case "Closed":
    case "Done":
    case "Automation Incomplete":
    case "Resolved (Pending Client Confirmation)":
      return true;

    default:
      return false;
  }
}

export function effortPercentage(x) {
  switch (x.EntityState.Name) {
    case "New":
    case "More Info Required":
    case "In Development":
      return 1;

    case "In Testing":
    case "Ready For Testing":
      return 0.5;

    case "Confirmed on Feature":
      return 0.25;

    case "Closed":
    case "Done":
    case "Automation Incomplete":
    case "Resolved (Pending Client Confirmation)":
      return 0.0;

    default:
      return 1.0;
  }
}

export function codeReviewStatus(entityState, value) {
  switch (entityState.Name) {
    case "New":
    case "More Info Required":
    case "In Development":
      return "";

    case "In Testing":
    case "Ready For Testing":
    case "Confirmed on Feature":
    case "Closed":
    case "Done":
    case "Automation Incomplete":
    case "Resolved (Pending Client Confirmation)":
      return value ? "" : "Pending";
    default:
      return "";
  }
}
