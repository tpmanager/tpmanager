import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as assignmentActions from '../../actions/assignmentActions';
import initialState from '../../reducers/initialState';
import { Label } from 'react-bootstrap';
import { getAssignments } from '../../api/iterationApi';

class Assignment extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            assignedId: this.props.assignedId,
            entityState: this.props.entityState,
            assignments: ""
        };
    }

    componentWillMount() {
        this.loadAssigned();
    }

    componentWillReceiveProps() {
        this.loadAssigned();
    }

    loadAssigned() {
        this.props.actions.loadAssignments(this.props.assignedId)
            .then(response => {
                if (response.ok) {
                    response.json().then(json => ({ json, response }))
                        .then(({ json, response }) => {
                            if (response.ok) {
                                const whoList = json.Items.map(user => {
                                    return this.whoIsAssigned(user.GeneralUser, user.Role, this.state.entityState);
                                });
                                let assignedList = whoList.sort(function (a, b) { return a.isResponsible == b.isResponsible ? 0 : a.isResponsible ? -1 : 1; }).map(assignedPerson => {
                                    if (assignedPerson.isResponsible) {
                                        return "<b>" + assignedPerson.userName + "</b>";
                                    }
                                    else {
                                        return "<div class='assigned-not-responsible'>" + assignedPerson.userName + "</div>";
                                    }
                                });
                                this.setState({ assignments: assignedList.join(", ") });
                            }
                        });
                }
            }
            ).catch(error => {
            });
    }

    getUserName(user) {
        return (user.FirstName == 'Mohammad') ? "Champ" : user.FirstName;
    }

    whoIsAssigned(user, role, entityState) {
        let returnValue = {
            userName: this.getUserName(user),
            isResponsible: false
        };
        switch (role.Name) {
            case "Quality Assurance Analyst":
                switch (entityState.Name) {
                    case "In Testing":
                    case "Ready For Testing":
                        {
                            returnValue.isResponsible = true;
                            return returnValue;
                        }
                }
                break;
            case "Software Developer":
                switch (entityState.Name) {
                    case "In Development":
                    case "Confirmed on Feature":
                        {
                            returnValue.isResponsible = true;
                            return returnValue;
                        }
                }
                break;
            case "Product Owner":
                switch (entityState.Name) {
                    case "More Info Required":
                    case "Under Review":
                        {
                            returnValue.isResponsible = true;
                            return returnValue;
                        }
                }
                break;
        }
        switch (entityState.Name) {
            case "Closed":
            case "Automation Incomplete":
                {
                    returnValue.isResponsible = true;
                    return returnValue;
                }
            case "New":
                {
                    returnValue.isResponsible = false;
                    return returnValue;
                }
        }
        return returnValue;
    }

    render() {
        if (this.state.assignments === "") {
            return null;
        }
        return (
            <div style={{ float: 'left' }}>
                Who : <span dangerouslySetInnerHTML={{ __html: this.state.assignments }} />
            </div>
        );
    }
}

Assignment.propTypes = {
    entityState: PropTypes.object.isRequired,
    assignedId: PropTypes.number.isRequired,
    actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        entityState: ownProps.entityState,
        assignedId: ownProps.assignedId
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(assignmentActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Assignment);
