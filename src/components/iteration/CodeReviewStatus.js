import React, { PropTypes } from 'react';
import * as util from './util';

const CodeReviewStatus = ({ status, entityState }) => {
  if (status.Value) return null;
  let reviewStatus = util.codeReviewStatus(entityState, status.Value);
  if (reviewStatus === '') return null;
  return (
    <div style={{ float: 'right' }}>
      C.R : <b>{reviewStatus}</b>
    </div>
  );
};

CodeReviewStatus.propTypes = {
  status: PropTypes.object.isRequired,
  entityState: PropTypes.object.isRequired
};

export default CodeReviewStatus;