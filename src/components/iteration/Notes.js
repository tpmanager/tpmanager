import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import * as firebase from 'firebase';

class Notes extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      teamId: this.props.teamId,
      teamIterationId: this.props.teamIterationId,
      notes: ''
    };

    this.onChange = this.onChange.bind(this);
    this.updateNotes = this.updateNotes.bind(this);
  }

  componentDidMount() {
    this.loadNotes();
    this.textarea.style.overflowY = 'hidden';
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.teamId && nextProps.teamIterationId) {
      this.setState({
        teamId: nextProps.teamId,
        teamIterationId: nextProps.teamIterationId,
        notes: ''
      }, () => {
        this.loadNotes();
      });
    }
  }

  loadNotes() {
    let ref = firebase.database().ref("Notes/" + this.state.teamId + "/" + this.state.teamIterationId);
    let _this = this;
    ref.once("value")
      .then(function (snapshot) {
        let val = snapshot.val();
        if (val) {
          _this.setState({
            notes: val.notes
          });
        }
        _this.resetHeight();
      });
  }

  updateNotes() {
    if (this.state.teamId) {
      let ref = firebase.database().ref("Notes/" + this.state.teamId);
      ref.child(this.state.teamIterationId).update({ notes: this.state.notes });
    }
  }

  onChange(event) {
    let _this = this;
    this.setState({ notes: event.target.value });
    this.resetHeight();
  }

  resetHeight() {
    this.textarea.style.height = 'auto';
    this.textarea.style.height = this.textarea.scrollHeight + 'px';
  }

  render() {
    return (
      <textarea {...this.props}
        className="form-control"
        value={this.state.notes}
        onBlur={this.updateNotes}
        onChange={this.onChange}
        width="100%"
        ref={(c) => this.textarea = c}></textarea>
    );
  }
}

Notes.propTypes = {
  teamId: PropTypes.string.isRequired,
  teamIterationId: PropTypes.string.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    teamId: ownProps.teamId,
    teamIterationId: ownProps.teamIterationId
  };
}

export default connect(mapStateToProps)(Notes);