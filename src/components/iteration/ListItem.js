import React, { PropTypes } from 'react';
import { Panel, Badge } from 'react-bootstrap';
import _ from 'underscore';

import CodeReviewStatus from './CodeReviewStatus';
import Assignment from './Assignment';
import { getResourceUrl } from '../../api/auth';

class ListItem extends React.Component {

  constructor(...args) {
    super(...args);
    this.openIcon = "▼";
    this.closeIcon = "►";

    this.state = {
      open: false,
      collapseIcon: this.closeIcon,
      ResourceTypeStyle: {
        float: "left",
        opacity: 0.8
      }
    };

    this.onPanelHeaderClick = this.onPanelHeaderClick.bind(this);
    this.onPanelItemClick = this.onPanelItemClick.bind(this);
  }

  componentWillMount() {
    if ((this.props.item.EntityState.Name != 'Closed' && this.props.item.EntityState.Name != 'Automation Incomplete') || this.props.item.ResourceType != "Bug") {
      this.setState({
        open: true,
        collapseIcon: this.openIcon
      });
    }
    if (this.props.item.ResourceType == "Bug") {
      this.setState({ ResourceTypeStyle: _.extend(this.state.ResourceTypeStyle, { color: 'Red', opacity: '0.5' }) });
    }
  }

  onPanelHeaderClick() {
    this.setState({
      open: !this.state.open,
      collapseIcon: (this.state.collapseIcon == this.closeIcon) ? this.openIcon : this.closeIcon
    });
  }

  onPanelItemClick() {
    window.open(getResourceUrl(this.props.item.ResourceType, this.props.item.Id), '_blank');
  }

  render() {
    let codeReviewStatus = this.props.item.CustomFields.find((e) => e.Name == "Code Review Completed");
    return (
      <Panel collapsible
        expanded={this.state.open}
        header={<div style={{ height: '10pt' }}>
          {this.ResourceTypeColor}
          <div style={{ float: 'left', cursor: 'pointer', width: '15pt' }} onClick={this.onPanelHeaderClick}>
            {this.state.collapseIcon}
          </div>
          <div style={{ float: 'left' }}>
            <b style={this.state.ResourceTypeStyle} >{this.props.item.ResourceType.replace("UserStory", "US ")}: &nbsp;</b>
            <div className={"tp-Number"} style={{ cursor: 'pointer', float: 'left' }} onClick={this.onPanelItemClick}>{this.props.item.Id}</div>

          </div>
          <Badge pullRight style={{ opacity: '0.5' }}> {this.props.item.Effort} </Badge>
          <div style={{ opacity: '0.7', float: 'right' }}> {this.props.item.EntityState.Name} &nbsp;&nbsp;</div>
        </div>
        }
        style={{ textAlign: 'left' }}
        bsStyle={this.props.cssStyle}>
        {this.props.item.Name}
        <br /><br /><Assignment assignedId={this.props.item.Id} entityState={this.props.item.EntityState} />
        <CodeReviewStatus status={codeReviewStatus} entityState={this.props.item.EntityState} />
      </Panel>
    );
  }
}

ListItem.propTypes = {
  item: PropTypes.object.isRequired,
  cssStyle: PropTypes.string.isRequired
};

export default ListItem;
