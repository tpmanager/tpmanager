import React, { PropTypes } from 'react';
import _ from 'underscore';
import { ListGroup } from 'react-bootstrap';

import ListItem from './ListItem';
import * as util from './util';

const IterationList = ({status, items}) => {

    let cssStyle;
    let filtered;
    switch (status) {
        case "TODO":
            cssStyle = "danger";
            filtered = _.filter(items, util.isTodoItem);
            break;
        case "INPROGRESS":
            cssStyle = "warning";
            filtered = _.filter(items, util.isInprogressItem);
            break;
        case "COMPLETED":
            cssStyle = "success";
            filtered = _.filter(items, util.isCompletedItem);
            break;
        default:
            cssStyle = "warning";
    }

    return (
        <ListGroup>
            {
                filtered.map(item =>
                    <ListItem key={item.Id} item={item} cssStyle={cssStyle} />
                )
            }
        </ListGroup>
    );
};

IterationList.propTypes = {
    items: PropTypes.array.isRequired,
    status: PropTypes.string.isRequired
};

export default IterationList;
