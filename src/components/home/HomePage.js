import React, { Component } from 'react';
import { Link } from 'react-router';
import DocumentTitle from 'react-document-title';

class HomePage extends Component {
  render() {
    return (
      <DocumentTitle title={`Home`}>
      <div className="container">
        <h3>Main Page</h3>
        <p>React, Redux, and React Router in EX6 for ultra-responsive web app.</p>
        <Link to="about" className="btn btn-primary btn-lg">Learn More</Link>
      </div>
      </DocumentTitle>
    );
  }
}

export default HomePage;