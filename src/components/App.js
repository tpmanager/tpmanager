import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Header from './common/Header';
import * as firebase from 'firebase';
import Config from '../firebase-config';

firebase.initializeApp(Config);

class App extends React.Component {
  render() {
    return (
      <div>
        <Header loading={this.props.loading}
          isAuthenticated={this.props.isAuthenticated}
          dispatch={this.props.dispatch}
          LoggedUser={this.props.LoggedUser} />
        {this.props.children}
      </div>
    );
  }
}

App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  children: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  LoggedUser: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    loading: state.ajaxCallsInProgress > 0,
    isAuthenticated: state.session.isAuthenticated,
    LoggedUser: state.session.LoggedUser
  };
}

export default connect(mapStateToProps)(App);