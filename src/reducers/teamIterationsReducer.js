import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function teamIterationsReducer(state = initialState.teamIterations, action) {
  switch (action.type) {
    case types.LOAD_TEAMITERATIONS_SUCCESS:
      return action.teamIterations;
    case types.RESET_TEAMITERATIONS:
      return initialState.teamIterations;
      
    default:
      return state;
  }
}