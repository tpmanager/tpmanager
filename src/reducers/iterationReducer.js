import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function iterationReducer(state = initialState.items, action) {
  switch (action.type) {
    case types.LOAD_ITERATION_SUCCESS:
      return action.items;
    case types.RESET_ITERATION:
      return initialState.items;

    default:
      return state;
  }
}
