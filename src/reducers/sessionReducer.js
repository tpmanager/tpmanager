import * as types from '../actions/actionTypes';
import initialState from './initialState';
import { isLoggedIn, getAcid, getLoggedUser, getCredential } from '../api/auth';
import { browserHistory } from 'react-router';

export default function sessionReducer(state = {
  isFetching: false,
  isAuthenticated: isLoggedIn() ? true : false,
  Acid: getAcid(),
  Credential: getCredential(),
  LoggedUser: getLoggedUser()
}, action) {

  switch (action.type) {
    case types.LOGIN_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isAuthenticated: false,
        user: action.creds
      });

    case types.LOGIN_SUCCESS:
      browserHistory.push('/iteration');
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: true,
        Acid: getAcid(),
        Credential: getCredential(),
        LoggedUser: getLoggedUser(),
        errorMessage: ''
      });

    case types.LOGIN_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: false,
        errorMessage: action.message
      });

    case types.LOGOUT_SUCCESS:
      browserHistory.push('/');
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: false
      });

    default:
      return state;
  }
}