import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function selectionReducer(state = initialState.selection, action) {
  switch (action.type) {
    case types.SET_SELECTION:
      return action.selection;
    default:
      return state;
  }
}