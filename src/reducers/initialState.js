export default {
  items: [],
  teams: [],
  teamIterations: [],
  selection: { teamId: "", teamIterationId: "" },
  ajaxCallsInProgress: 0
};
