import { combineReducers } from 'redux';
import items from './iterationReducer';
import teams from './teamReducer';
import teamIterations from './teamIterationsReducer';
import selection from './selectionReducer';
import session from './sessionReducer';
import ajaxCallsInProgress from './ajaxStatusReducer';

const rootReducer = combineReducers({
  items: items,
  teams: teams,
  teamIterations: teamIterations,
  selection: selection,
  session: session,
  ajaxCallsInProgress: ajaxCallsInProgress
});

export default rootReducer;