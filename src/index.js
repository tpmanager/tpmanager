import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { Provider } from 'react-redux';

import routes from './routes';
import configureStore from './store/configureStore';
import { loadTeams } from './actions/teamActions';
import * as auth from './api/auth';

import './styles/styles.css';//Webpack can import CSS files too!

const store = configureStore();
if (auth.isLoggedIn()) {
  store.dispatch(loadTeams());
}

render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>,
  document.getElementById('app')
);

let timeoutHandle = function () {
  window.setTimeout(
    function () {
      $("nav").show(800);
    }, 500);
};

$(document).ready(function () {
  $("#top-menu-bar")
    .mouseenter(timeoutHandle)
    .mouseleave(
    function () {
      $("nav").hide(500);
      window.clearTimeout(timeoutHandle);
    }
    );
  $(document).mousemove(function () {
    if (!$("#top-menu-bar").is(':hover')) {
      $("nav").hide();
    }
  });

});
